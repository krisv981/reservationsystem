﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReservationSystem.DATA.EF;
using System.Linq.Expressions;




namespace ReservationSystem.DOMAIN.Repos
{
    //public class GenericRepository<PLACEHOLDER> and swappable placeholder is a class
    //v1--this is the version before interface
    //public class GenericRepository<TEntity> where TEntity : class

    //v2--generic repo now implements an interface (best practice)
    public class GenericRepository<TEntity> : IGenericRepositable<TEntity> where TEntity : class
    {
        /* Generics: make it possible in C# to write code that is NOT specific
         * to any ONE entity, so it can be used with MANY DIFFERENT entities.
         * 
         * AKA, lets you swap out the type.
         * 
         * This repo will be available for use by ANY domain model (entity).
         * 
         * */

        //vA -- before unit of Work
        //private cStoreEntities db = new cStoreEntities();

        //vB ctor with unit of work
        internal DbContext db;
        public GenericRepository(DbContext context)
        {
            this.db = context;
        }


        //this basic Get() doesn't work well with tables with lookups
        //public List<TEntity> Get()
        //{
        //    return db.Set<TEntity>().ToList();
        //}

        //this advanced Get() accommodates any table type
        //if you include comma-separated list of table names, those
        //lookup table records are included in the returned records
        public virtual List<TEntity> Get(string includeProperties = "")
        {
            IQueryable<TEntity> query = db.Set<TEntity>();

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return query.ToList();
        }


        public TEntity Find(object id)
        {
            return db.Set<TEntity>().Find(id);
        }

        public void Add(TEntity entity)
        {
            db.Set<TEntity>().Add(entity);
            //db.SaveChanges();//Unit of Work allows us to save db changes when needed
        }

        public void Update(TEntity entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            //db.SaveChanges();
        }

        public void Remove(TEntity entity)
        {
            db.Set<TEntity>().Remove(entity);
            //db.SaveChanges();
        }

        public void Remove(object id)
        {
            TEntity entity = Find(id);
            db.Set<TEntity>().Remove(entity);
            //db.SaveChanges();
        }

        public int Count()
        {
            return Get().Count();
        }

        public IQueryable<TEntity> Search(Expression<Func<TEntity, bool>> predicate)
        {
            return db.Set<TEntity>().AsExpandable().Where(predicate);
        }

        #region DISPOSE CODE
        //--this method deletes the db object as needed
        //--NOTE WE ADDED SOME CODE TO ACCOUNT FOR THE DISPOSE() being
        //--outside of the controller
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }




        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #endregion
    }
    }
