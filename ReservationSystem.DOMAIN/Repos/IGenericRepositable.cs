﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;//added

namespace ReservationSystem.DOMAIN.Repos
{
    public interface IGenericRepositable<TEntity> : IDisposable where TEntity : class
    {
        /* code syntax template for interface:
         * interface ISomethingable 
         * {
         *   //method stubs that must be implemented by implementing classes
         *   returnType MethodName1(parameter list);
         * }
         * 
         * code syntax template for class implementing an interface
         * class1 : ISomethingable
         * {
         *      //implementing interface's method stubs
         *      returnType MethodName1(parameter list)
         *      {
         *         //code to make MethodName1 work and return appropriate-type value 
         *      }
         * }
         * 
         * 
         * */

        //method stub doesn't define functionality. ends in semi-colon
        List<TEntity> Get(string includeProperties = "");
        TEntity Find(object id);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(TEntity entity);

        //overload to make it easier to access desired record to remove
        void Remove(object id);

        //what if we add a new method that wasn't already being used by
        //the implementing class GenericRepository?
        int Count();

        #region Search
        /* GOAL: Implement a general search method that will be available 
         * across all our repositories.
         * 
         * 1) Add the requirement to the interface (here)
         * 
         * 2) Add the method to the Generic Repo so it's available to all 
         * inheriting members (type-specific repos).
         * 
         * 3) Install the LinqKit Nuget package to the UI and Domain 
         * where the search is executed and defined accordingly.
         *  --dynamically generate SQL statements thru the use of the 
         *  Func<> parameter
         *  
         *  --Func<> takes 2 arguments, 1st is the class/model being queried
         *  and the 2nd is a boolean required by LINQ.
         *  
         *  --LinqKit has a method called AsExpandable() - when we query
         *  with multiple parameters this helps us avoid the 
         *  exception "The LINQ expression node type 'invoke' is not
         *  supported in LinqToEntities".
         * */

        //declare the search method signature/stub
        IQueryable<TEntity> Search(Expression<Func<TEntity, bool>> predicate);

        #endregion

    }
}
