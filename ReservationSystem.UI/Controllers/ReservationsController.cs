﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReservationSystem.DATA.EF;
using Microsoft.AspNet.Identity;
using System.Drawing;
using ReservationSystem.DATA.EF.Services;

namespace ReservationSystem.UI.Controllers
{
    public class ReservationsController : Controller
    {
        private ReservationSystemEntities db = new ReservationSystemEntities();

        // GET: Reservations
        [Authorize]
        public ActionResult Index()
        {

            var currentUser = User.Identity.GetUserId();
            if (User.IsInRole("Admin") || User.IsInRole("Employee"))
            {
                var reservations = db.Reservations.Include(r => r.Location).Include(r => r.OwnerAsset);
                return View(reservations.ToList());
            }
            else
            {
                var reservations = db.Reservations.Include(r => r.Location).Include(r => r.OwnerAsset).Where(x => x.OwnerAsset.OwnerID == currentUser);
                return View(reservations.ToList());

            }
        }

        // GET: Reservations/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        // GET: Reservations/Create
        [Authorize(Roles ="Admin, User")]
        public ActionResult Create()
        {
            if (Request.IsAuthenticated && User.IsInRole("User"))
            {
                var currentUser = User.Identity.GetUserId();
                ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "LocationName");
                ViewBag.OwnerAssetID = new SelectList(db.OwnerAssets.Where(x => (x.OwnerID == currentUser)), "OwnerAssetID", "AssetName");
            }
            if (Request.IsAuthenticated && User.IsInRole("Admin"))
            {
                ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "LocationName");
                ViewBag.OwnerAssetID = new SelectList(db.OwnerAssets, "OwnerAssetID", "AssetName");
            }
            return View();
        }


        // POST: Reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "ReservationID,LocationID,ReservationDate,OwnerAssetID")] Reservation reservation)
        {


            Location desiredLocation = db.Locations.Where(x => x.LocationID == reservation.LocationID).Single();
            DateTime desiredDate = reservation.ReservationDate;
            int resLimit = desiredLocation.ReservationLimit;
            int currentReservations = db.Reservations.Where(x => (x.LocationID == desiredLocation.LocationID) && (x.ReservationDate == desiredDate)).Count();
            var currentUser = User.Identity.GetUserId();
            int currentUserReservations = db.Reservations.Where(x => (x.OwnerAssetID == reservation.OwnerAssetID) && (x.ReservationDate == desiredDate)).Count();
            if (ModelState.IsValid)
            {
                if (currentReservations < resLimit && !User.IsInRole("Admin"))
                {
                    if (currentUserReservations == 0)
                    {
                        db.Reservations.Add(reservation);
                        db.SaveChanges();
                        ViewBag.Message = null;
                        return RedirectToAction("Index");
                    }
                    ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "LocationName", reservation.LocationID);
                    ViewBag.OwnerAssetID = new SelectList(db.OwnerAssets, "OwnerAssetID", "AssetName", reservation.OwnerAssetID);

                    ViewBag.Message = "This User has already been booked for this date";

                    return View(reservation);

                }
                if (currentUserReservations == 0 && User.IsInRole("Admin"))
                {
                    db.Reservations.Add(reservation);
                    db.SaveChanges();
                    ViewBag.Message = null;
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "LocationName", reservation.LocationID);
                    ViewBag.OwnerAssetID = new SelectList(db.OwnerAssets, "OwnerAssetID", "AssetName", reservation.OwnerAssetID);
                    ViewBag.Message = "This location is overbooked";
                    return View(reservation);

                }

            }

            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "LocationName", reservation.LocationID);
            ViewBag.OwnerAssetID = new SelectList(db.OwnerAssets, "OwnerAssetID", "AssetName", reservation.OwnerAssetID);
            return View(reservation);
        }

        // GET: Reservations/Edit/5
        [Authorize(Roles = "Admin, Employee")]
        public ActionResult Edit(int? id)
        {
            var currentUser = User.Identity.GetUserId();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "LocationName", reservation.LocationID);
            ViewBag.OwnerAssetID = new SelectList(db.OwnerAssets, "OwnerAssetID", "AssetName", reservation.OwnerAssetID);
            return View(reservation);
        }

        // POST: Reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Employee")]
        public ActionResult Edit([Bind(Include = "ReservationID,LocationID,ReservationDate,OwnerAssetID")] Reservation reservation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reservation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "LocationName", reservation.LocationID);
            ViewBag.OwnerAssetID = new SelectList(db.OwnerAssets, "OwnerAssetID", "AssetName", reservation.OwnerAssetID);


            return View(reservation);
        }

        // GET: Reservations/Delete/5
        [Authorize(Roles = "Admin, Employee")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        // POST: Reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Employee")]
        public ActionResult DeleteConfirmed(int id)
        {
            Reservation reservation = db.Reservations.Find(id);
            db.Reservations.Remove(reservation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
