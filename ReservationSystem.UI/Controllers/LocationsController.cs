﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReservationSystem.DATA.EF;
using System.Drawing;
using ReservationSystem.DATA.EF.Services;

namespace ReservationSystem.UI.Controllers
{
    public class LocationsController : Controller
    {
        private ReservationSystemEntities db = new ReservationSystemEntities();

        // GET: Locations
        public ActionResult Index()
        {
            return View(db.Locations.ToList());
        }

        // GET: Locations/Details/5
      
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // GET: Locations/Create
        [Authorize(Roles ="Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Locations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles ="Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LocationID,LocationName,LocationPhoto,Address,City,State,ZipCode,ReservationLimit")] Location location, HttpPostedFileBase imageUpload)
        {
            #region File Upload using Image Service
            string imageName = "noimage.png";//default image filename

            if (imageUpload != null)
            {
                //process the image upload if it exists...
                imageName = imageUpload.FileName;
                string imgExt = imageName.Substring(imageName.LastIndexOf("."));
                string[] allowedExtensions = { ".png", ".jpg", ".jpeg", ".gif" };
                if (allowedExtensions.Contains(imgExt.ToLower()))
                {
                    //make sure it's unique... like:
                    imageName = Guid.NewGuid() + imgExt;

                    //OR datetimestamp + bookname
                    //imageName = (DateTime.Now.ToString("yyyyMMddHHmmss") + "_"
                    //    + title.BookTitle).Substring(0, 100);

                    //---Prepare the variables for image service ResizeImage()--
                    //set up the server path for where to upload (folder path only)
                    string folderSavePath = Server.MapPath("~/Content/Images/Uploads/");

                    //convert the uploaded file from HttpPostedFileBase to Image 
                    //(requires System.Drawing namespace)
                    Image convertedImage = Image.FromStream(imageUpload.InputStream);

                    //set max size for full-size image (in pixels)
                    int maxImageSize = 500;

                    //set max size for thumbnail image
                    int maxThumbSize = 100;

                    //----call image service to resize & save
                    ImageServices.ResizeImage(folderSavePath, imageName, convertedImage
                        , maxImageSize, maxThumbSize);
                }
                else
                {
                    //if the user uploads an unacceptable file type, throw exception 
                    //throw new InvalidFileTypeException("Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed");
                    ViewBag.Message = "Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed";
                    return View(location);
                }
            }

            //regardless of whether we had an image upload, update the BookImage
            //property of the new title record before adding it to the DB.
            ViewBag.Message = null;
            location.LocationPhoto = imageName;

            #endregion

            if (ModelState.IsValid)
            {
                db.Locations.Add(location);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(location);
        }

        // GET: Locations/Edit/5        
        [Authorize(Roles ="Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: Locations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles ="Admin")]
        public ActionResult Edit([Bind(Include = "LocationID,LocationName,LocationPhoto,Address,City,State,ZipCode,ReservationLimit")] Location location, HttpPostedFileBase imageUpload)
        {
            if (ModelState.IsValid)

            {
                #region File Upload using Image Service
                string imageName = "noimage.png";//default image filename

                if (imageUpload != null)
                {
                    //process the image upload if it exists...
                    imageName = imageUpload.FileName;
                    string imgExt = imageName.Substring(imageName.LastIndexOf("."));
                    string[] allowedExtensions = { ".png", ".jpg", ".jpeg", ".gif" };
                    if (allowedExtensions.Contains(imgExt.ToLower()))
                    {
                        //make sure it's unique... like:
                        imageName = Guid.NewGuid() + imgExt;

                        //OR datetimestamp + bookname
                        //imageName = (DateTime.Now.ToString("yyyyMMddHHmmss") + "_"
                        //    + title.BookTitle).Substring(0, 100);

                        //---Prepare the variables for image service ResizeImage()--
                        //set up the server path for where to upload (folder path only)
                        string folderSavePath = Server.MapPath("~/Content/Images/Uploads/");

                        //convert the uploaded file from HttpPostedFileBase to Image 
                        //(requires System.Drawing namespace)
                        Image convertedImage = Image.FromStream(imageUpload.InputStream);

                        //set max size for full-size image (in pixels)
                        int maxImageSize = 500;

                        //set max size for thumbnail image
                        int maxThumbSize = 100;

                        //----call image service to resize & save
                        ImageServices.ResizeImage(folderSavePath, imageName, convertedImage
                            , maxImageSize, maxThumbSize);
                    }
                    else
                    {
                        //if the user uploads an unacceptable file type, throw exception 
                        //throw new InvalidFileTypeException("Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed");
                        ViewBag.Message = "Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed";
                        return View(location);
                    }
                }
                else
                {
                    imageName = location.LocationPhoto;
                }

                //regardless of whether we had an image upload, update the BookImage
                //property of the new title record before adding it to the DB.
                ViewBag.Message = null;
                location.LocationPhoto = imageName;

                #endregion

                db.Entry(location).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(location);
        }

        // GET: Locations/Delete/5
        [Authorize(Roles ="Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: Locations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles ="Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Location location = db.Locations.Find(id);
            db.Locations.Remove(location);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
