﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReservationSystem.DATA.EF;
using Microsoft.AspNet.Identity;
using IdentitySample.Models;
using System.Drawing;
using ReservationSystem.DATA.EF.Services;

namespace ReservationSystem.UI.Controllers
{
    public class MyAuthorize : AuthorizeAttribute
    {
        private ReservationSystemEntities db = new ReservationSystemEntities();

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                return false;
            }
            var rd = httpContext.Request.RequestContext.RouteData;
            var id = rd.Values["id"];
            var userName = httpContext.User.Identity.Name;
            OwnerAsset ownerAsset = db.OwnerAssets.Find(id);
            UserDetail user = db.UserDetails.Find(userName);
            return ownerAsset.OwnerID == user.UserID;
        }
    }
    public class OwnerAssetsController : Controller
    {
        
        private ReservationSystemEntities db = new ReservationSystemEntities();

        // GET: OwnerAssets
        [Authorize]
        public ActionResult Index()
        {
            var teamlist = db.OwnerAssets.ToList().Where(x => x.OwnerID == User.Identity.GetUserId());

            return View(teamlist);
        }
        [Authorize(Roles ="Admin, Employee")]
        public ActionResult AdminIndex()
        {
            return View(db.OwnerAssets.ToList());
        }

        // GET: OwnerAssets/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OwnerAsset ownerAsset = db.OwnerAssets.Find(id);
            if (ownerAsset == null)
            {
                return HttpNotFound();
            }
            return View(ownerAsset);
        }
        [Authorize]
        // GET: OwnerAssets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OwnerAssets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "OwnerAssetID,AssetName,OwnerID,AssetPhoto,SpecialNotes,IsActive,DateAdded")] OwnerAsset ownerAsset, HttpPostedFileBase imageUpload)
        {
            if (ModelState.IsValid)
            {
                #region File Upload using Image Service
                string imageName = "noimage.png";//default image filename

                if (imageUpload != null)
                {
                    //process the image upload if it exists...
                    imageName = imageUpload.FileName;
                    string imgExt = imageName.Substring(imageName.LastIndexOf("."));
                    string[] allowedExtensions = { ".png", ".jpg", ".jpeg", ".gif" };
                    if (allowedExtensions.Contains(imgExt.ToLower()))
                    {
                        //make sure it's unique... like:
                        imageName = Guid.NewGuid() + imgExt;

                        //OR datetimestamp + bookname
                        //imageName = (DateTime.Now.ToString("yyyyMMddHHmmss") + "_"
                        //    + title.BookTitle).Substring(0, 100);

                        //---Prepare the variables for image service ResizeImage()--
                        //set up the server path for where to upload (folder path only)
                        string folderSavePath = Server.MapPath("~/Content/Images/Uploads/");

                        //convert the uploaded file from HttpPostedFileBase to Image 
                        //(requires System.Drawing namespace)
                        Image convertedImage = Image.FromStream(imageUpload.InputStream);

                        //set max size for full-size image (in pixels)
                        int maxImageSize = 500;

                        //set max size for thumbnail image
                        int maxThumbSize = 100;

                        //----call image service to resize & save
                        ImageServices.ResizeImage(folderSavePath, imageName, convertedImage
                            , maxImageSize, maxThumbSize);
                    }
                    else
                    {
                        //if the user uploads an unacceptable file type, throw exception 
                        //throw new InvalidFileTypeException("Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed");
                        ViewBag.Message = "Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed";
                        return View(ownerAsset);
                    }
                }

                //regardless of whether we had an image upload, update the BookImage
                //property of the new title record before adding it to the DB.
                ViewBag.Message = null;
                ownerAsset.AssetPhoto = imageName;

                #endregion

                ownerAsset.OwnerID = User.Identity.GetUserId();
                ownerAsset.DateAdded = DateTime.UtcNow;
                db.OwnerAssets.Add(ownerAsset);
                db.SaveChanges();
                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction("AdminIndex");
                }
                else
                {
                return RedirectToAction("Index");
                }
            }

            return View(ownerAsset);
        }

        // GET: OwnerAssets/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OwnerAsset ownerAsset = db.OwnerAssets.Find(id);
            if (ownerAsset == null)
            {
                return HttpNotFound();
            }
            var currentUser = User.Identity.GetUserId();
            ViewBag.OwnerAssetID = new SelectList(db.OwnerAssets.Where(x => (x.OwnerID == currentUser)), "OwnerAssetID", "AssetName");
            return View(ownerAsset);
        }

        // POST: OwnerAssets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "OwnerAssetID,AssetName,OwnerID,AssetPhoto,SpecialNotes,IsActive,DateAdded")] OwnerAsset ownerAsset, HttpPostedFileBase imageUpload)
        {
            if (ModelState.IsValid)
            {

                #region File Upload using Image Service
                string imageName = "noimage.png";//default image filename

                if (imageUpload != null)
                {
                    //process the image upload if it exists...
                    imageName = imageUpload.FileName;
                    string imgExt = imageName.Substring(imageName.LastIndexOf("."));
                    string[] allowedExtensions = { ".png", ".jpg", ".jpeg", ".gif" };
                    if (allowedExtensions.Contains(imgExt.ToLower()))
                    {
                        //make sure it's unique... like:
                        imageName = Guid.NewGuid() + imgExt;

                        //OR datetimestamp + bookname
                        //imageName = (DateTime.Now.ToString("yyyyMMddHHmmss") + "_"
                        //    + title.BookTitle).Substring(0, 100);

                        //---Prepare the variables for image service ResizeImage()--
                        //set up the server path for where to upload (folder path only)
                        string folderSavePath = Server.MapPath("~/Content/Images/Uploads/");

                        //convert the uploaded file from HttpPostedFileBase to Image 
                        //(requires System.Drawing namespace)
                        Image convertedImage = Image.FromStream(imageUpload.InputStream);

                        //set max size for full-size image (in pixels)
                        int maxImageSize = 500;

                        //set max size for thumbnail image
                        int maxThumbSize = 100;

                        //----call image service to resize & save
                        ImageServices.ResizeImage(folderSavePath, imageName, convertedImage
                            , maxImageSize, maxThumbSize);
                    }
                    else
                    {
                        //if the user uploads an unacceptable file type, throw exception 
                        //throw new InvalidFileTypeException("Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed");
                        ViewBag.Message = "Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed";
                        return View(ownerAsset);
                    }
                }
                else
                {
                    imageName = ownerAsset.AssetPhoto;
                }

                //regardless of whether we had an image upload, update the BookImage
                //property of the new title record before adding it to the DB.
                ViewBag.Message = null;
                ownerAsset.AssetPhoto = imageName;

                #endregion
                if (User.IsInRole("User"))
                {
                ownerAsset.OwnerID = User.Identity.GetUserId();
                ownerAsset.DateAdded = DateTime.UtcNow;
                }
                db.Entry(ownerAsset).State = EntityState.Modified;
                db.SaveChanges();

                if (User.IsInRole("Admin")||User.IsInRole("Employee"))
                {
                    return RedirectToAction("AdminIndex");
                }
                else
                {
                return RedirectToAction("Index");
                }
            }
            return View(ownerAsset);
        }

        // GET: OwnerAssets/Delete/5
        [Authorize(Roles ="Admin, Employee")]        
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OwnerAsset ownerAsset = db.OwnerAssets.Find(id);
            if (ownerAsset == null)
            {
                return HttpNotFound();
            }
            return View(ownerAsset);
        }

        // POST: OwnerAssets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles ="Admin, Employee")]
        public ActionResult DeleteConfirmed(int id)
        {
            OwnerAsset ownerAsset = db.OwnerAssets.Find(id);
            string folderSavePath = Server.MapPath("~/Content/Images/Books/");
            ImageServices.Delete(folderSavePath, ownerAsset.AssetPhoto);

            db.OwnerAssets.Remove(ownerAsset);
            db.SaveChanges();
            if (User.IsInRole("Admin")|| User.IsInRole("Employee"))
            {
                return RedirectToAction("AdminIndex");               
            }
            else
            {
            return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        ////ajax methods
        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult AjaxDelete(int id)
        //{
        //    OwnerAsset ownerAsset = db.OwnerAssets.Find(id);
        //    db.OwnerAssets.Remove(ownerAsset);
        //    db.SaveChanges();
        //    var message = string.Format($"Deleted Team {ownerAsset.AssetName} from the database.");
        //    return Json(new
        //    {
        //        id = id,
        //        message = message

        //    });
        //}
        ////partial view for ajax display
        //public PartialViewResult OwnerAssetDetails (int id)
        //{
        //    OwnerAsset ownerAsset = db.OwnerAssets.Find(id);
        //    return PartialView(ownerAsset);
        //}

        ////ajax add
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult AjaxCreate(OwnerAsset ownerAsset, HttpPostedFileBase imageUpload) //need to add file upload stuff here too
        //{
        //    #region File Upload using Image Service
        //    string imageName = "noimage.png";//default image filename

        //    if (imageUpload != null)
        //    {
        //        //process the image upload if it exists...
        //        imageName = imageUpload.FileName;
        //        string imgExt = imageName.Substring(imageName.LastIndexOf("."));
        //        string[] allowedExtensions = { ".png", ".jpg", ".jpeg", ".gif" };
        //        if (allowedExtensions.Contains(imgExt.ToLower()))
        //        {
        //            //make sure it's unique... like:
        //            imageName = Guid.NewGuid() + imgExt;

        //            //OR datetimestamp + bookname
        //            //imageName = (DateTime.Now.ToString("yyyyMMddHHmmss") + "_"
        //            //    + title.BookTitle).Substring(0, 100);

        //            //---Prepare the variables for image service ResizeImage()--
        //            //set up the server path for where to upload (folder path only)
        //            string folderSavePath = Server.MapPath("~/Content/Images/Uploads/");

        //            //convert the uploaded file from HttpPostedFileBase to Image 
        //            //(requires System.Drawing namespace)
        //            Image convertedImage = Image.FromStream(imageUpload.InputStream);

        //            //set max size for full-size image (in pixels)
        //            int maxImageSize = 500;

        //            //set max size for thumbnail image
        //            int maxThumbSize = 100;

        //            //----call image service to resize & save
        //            ImageServices.ResizeImage(folderSavePath, imageName, convertedImage
        //                , maxImageSize, maxThumbSize);
        //        }
        //        else
        //        {
        //            //if the user uploads an unacceptable file type, throw exception 
        //            //throw new InvalidFileTypeException("Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed");
        //            ViewBag.Message = "Invalid File Type Uploaded. Only .png, .jpg, .jpeg, and .gif file types are allowed";
        //            return View(ViewBag.Message);
        //        }
        //    }
        //    else
        //    {
        //        imageName = ownerAsset.AssetPhoto;
        //    }

        //    //regardless of whether we had an image upload, update the BookImage
        //    //property of the new title record before adding it to the DB.
        //    ownerAsset.AssetPhoto = imageName;

        //    #endregion

        //    db.OwnerAssets.Add(ownerAsset);
        //    db.SaveChanges();
        //    return Json(ownerAsset);
        //}
        ////httpget to get the record to update
        //[HttpGet]
        //public PartialViewResult OwnerAssetEdit(int id)
        //{
        //    OwnerAsset ownerAsset = db.OwnerAssets.Find(id);
        //    return PartialView(ownerAsset);
        //}
        ////http post to make changes
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult AjaxEdit(OwnerAsset ownerAsset)
        //{
        //    db.Entry(ownerAsset).State = EntityState.Modified;
        //    db.SaveChanges();
        //    return Json(ownerAsset);

        //}



    }
}
