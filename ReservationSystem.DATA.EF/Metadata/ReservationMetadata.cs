﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.DATA.EF/*.Metadata*/
{
    public class ReservationMetadata
    {
        // public int ReservationID { get; set; }

        [Display(Name = "Reservation Date")]
        [DisplayFormat(DataFormatString ="{0:d}")]
        [Required]
        public System.DateTime ReservationDate { get; set; }

        [Required]
        [Display(Name ="Location Name")]
        public int LocationID { get; set; }


        [Display(Name = "Team Name")]
        [Required]
        public int OwnerAssetID { get; set; }

    }

    [MetadataType(typeof(ReservationMetadata))]
    public partial class Reservation { }
}
