﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace ReservationSystem.DATA.EF
{
    public class LocationMetadata
    {
        public int LocationID { get; set; }
        [Required]
        [StringLength(50, ErrorMessage ="50 Characters Max")]
        [Display(Name ="Name")]
        public string LocationName { get; set; }
        [Display(Name ="Photo")]
        public string LocationPhoto { get; set; }
        [Required]
        [StringLength(100,ErrorMessage ="100 Characters Max")]
        public string Address { get; set; }
        [Required]
        [StringLength(50,ErrorMessage ="50 Characters Max")]
        public string City { get; set; }
        [Required]
        [StringLength(2, ErrorMessage = "2 Characters Max")]
        public string State { get; set; }
        [Required]
        [StringLength(5, ErrorMessage = "5 Characters Max")]
        [Display(Name ="Zip")]
        public string ZipCode { get; set; }
        [Required]
        [Display(Name ="Reservation Limit")]
        public byte ReservationLimit { get; set; }
    }
    [MetadataType(typeof(LocationMetadata))]
    public partial class Location { }
}
