﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.DATA.EF/*.Metadata*/
{
    public class OwnerAssetMetadata
    {
        
        public int OwnerAssetID { get; set; }
        [Required]
        [Display(Name ="Team Name")]
        public string AssetName { get; set; }
        


        public string OwnerID { get; set; }


        [Display(Name ="Team Picture")]
        [DisplayFormat(NullDisplayText ="No Image")]
        public string AssetPhoto { get; set; }
        [UIHint("MultilineText")]
        [DisplayFormat(NullDisplayText ="No Description")]
        [Display(Name ="Description")]
        public string SpecialNotes { get; set; }
        [Display(Name ="Active")]
        public bool IsActive { get; set; }

        private DateTime dateAdded;
        [DisplayFormat(DataFormatString ="{0:d}")]
        public DateTime DateAdded
        {
            get {return dateAdded=DateTime.UtcNow; }
            set {dateAdded = value;}
        }

    }
    [MetadataType(typeof(OwnerAssetMetadata))]
    public partial class OwnerAsset { }
}
